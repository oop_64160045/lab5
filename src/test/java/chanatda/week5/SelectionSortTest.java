package chanatda.week5;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class SelectionSortTest {

    @Test
    public void shouldFindMininIndexTestCase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int pos = 0;
        int MinIndex = SelectionsortApp.findMinIndex(arr, pos);
        assertEquals(4, MinIndex);
    }

    @Test
    public void shouldFindMininIndexTestCase2() {
        int arr[] = { 1, 4, 3, 2, 5 };
        int pos = 1;
        int MinIndex = SelectionsortApp.findMinIndex(arr, pos);
        assertEquals(3, MinIndex);
    }

    @Test
    public void shouldFindMininIndexTestCase3() {
        int arr[] = { 1, 2, 3, 4, 5 };
        int pos = 2;
        int MinIndex = SelectionsortApp.findMinIndex(arr, pos);
        assertEquals(2, MinIndex);
    }

    @Test
    public void shouldFindMininIndexTestCase4() {
        int arr[] = { 1, 1, 1, 1, 1, 0, 1, 1 };
        int pos = 0;
        int MinIndex = SelectionsortApp.findMinIndex(arr, pos);
        assertEquals(5, MinIndex);
    }

    @Test
    public void shouldSwapTestcase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 1, 4, 3, 2, 5 };
        int frist = 0;
        int second = 4;
        SelectionsortApp.Swap(arr, frist, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldSwapTestcase2() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int expected[] = { 5, 4, 3, 2, 1 };
        int frist = 0;
        int second = 0;
        SelectionsortApp.Swap(arr, frist, second);
        assertArrayEquals(expected, arr);
    }

    @Test
    public void shouldSelectionSortTestcase1() {
        int arr[] = { 5, 4, 3, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5 };
        SelectionsortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);

    }

    @Test
    public void shouldSelectionSortTestcase2() {
        int arr[] = { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionsortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);

    }

    @Test
    public void shouldSelectionSortTestcase3() {
        int arr[] = { 6, 9, 3, 7, 10, 5, 4, 8, 2, 1 };
        int sortedArr[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
        SelectionsortApp.selectionSort(arr);
        assertArrayEquals(sortedArr, arr);
    }
}